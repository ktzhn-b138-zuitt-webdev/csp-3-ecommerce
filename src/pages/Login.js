import { Fragment, useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Nav, Row, Col } from 'react-bootstrap';
import { Redirect, useHistory, Link, NavLink } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Banner from '../components/Banner';

export default function Login(props) {

        // Allows us to consume the User context object and it's properties to use for user validation
        const {user, setUser} = useContext(UserContext);
            const history = useHistory();
		// State hooks to store the values of the input fields
		const [email, setEmail] = useState('');
	    const [password, setPassword] = useState('');
	    // State to determine whether submit button is enabled or not
	    const [isActive, setIsActive] = useState(false);

	    function authenticate(e) {

	        // Prevents page redirection via form submission
	        e.preventDefault();

            // Fetch request to process the backend API
            // Syntax: fetch('url', {options})
            // .then(res => res.json())
            // .then(data => {})
            fetch('https://desolate-dusk-90349.herokuapp.com/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // If no user information is found, the "access" property will not be available and will return undefined
                if(typeof data.access !== "undefined"){
                    // The token will be used to retrieve user information across the whole frontend application and storing it in the localStorage to allow ease of access to the user's information
                    localStorage.setItem('token', data.access);

                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "You have successfully logged in."
                    });


                history.push("/products");
                window.location.reload(true);
                }
                else{
                    Swal.fire({
                        title: "Authentication failed",
                        icon: "error",
                        text: "Invalid user details."
                    })
                }

            })

	        setEmail('');
	        setPassword('');

	    }

		useEffect(() => {

	        // Validation to enable submit button when all fields are populated and both passwords match
	        if(email !== '' && password !== ''){
	            setIsActive(true);
	        }else{
	            setIsActive(false);
	        }

	    }, [email, password]);

                const data={
                    title: "Happy Café",
                    content: "",

                }
    return (

        <Fragment className="container logreg">
        <Banner data={data}/>
    <Card className="text-center">
    <Row>
        <Col></Col>
        <Col  xs={6} >          
        <Card.Header> </Card.Header>
          <Card.Body>
            <Card.Title className="pb-3">Login</Card.Title>
            <Card.Text>
              <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email" 
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password" className="mb-3">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Password" 
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>
                { isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn" className="btnpress">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" className="btnpress" disabled>
                        Submit
                    </Button>
                }
            </Form>    
            </Card.Text>
          </Card.Body>
          <Card.Footer className="text-muted mt-3">Haven't made an account yet?  <Nav.Link as={NavLink} to="/register" exact>Register here!</Nav.Link></Card.Footer>
  </Col>
        <Col></Col>
  </Row>
    </Card>
            
    </Fragment>   )
}
