import { Fragment, useState, useEffect, useContext } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import CartButton from '../components/CartButton';
import { Container, Row, Col, Carousel } from 'react-bootstrap';
import UserContext from '../UserContext';
import {Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Home(){
	const {user, setUser} = useContext(UserContext);

	const data={
		title: "Happy Café",
		content: "Great food and drinks at great prices."
	}

	return(
		<Fragment>
			<Banner data={data}/>
			<hr/>
			<Container>
			  <Row>
			    <Col>
			    <Carousel fade>
			      <Carousel.Item>
			        <img
			          className="d-block w-100"
			          src="http://lorempixel.com/400/200/food/Happy Cafe?text=First slide&bg=373940"
			          alt="First slide"
			        />
			        <Carousel.Caption>
			          <h3>Healthy ingredients</h3>
			          <p>Always sourced from the freshest local sources</p>
			        </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="d-block w-100"
			          src="http://lorempixel.com/400/200/food/Happy Cafe?text=Second slide&bg=282c34"
			          alt="Second slide"
			        />

			        <Carousel.Caption>
			          <h3>Delicious yet affordable</h3>
			          <p>You don't need to spend too much money to get good food</p>
			        </Carousel.Caption>
			      </Carousel.Item>
			      <Carousel.Item>
			        <img
			          className="d-block w-100"
			          src="http://lorempixel.com/400/200/food/Happy Cafe?text=Third slide&bg=20232a"
			          alt="Third slide"
			        />

			        <Carousel.Caption>
			          <h3>Drinks prepared fresh</h3>
			          <p>Guaranteed cold and ready to drink.</p>
			        </Carousel.Caption>
			      </Carousel.Item>
			    </Carousel>
			    </Col>
			    <Col>
				    <h2> About Happy Cafe </h2>
				    <ul>
				    <li>Drinks prepared fresh</li>
				    <li>Delicious yet affordable</li>
				    <li>Healthy ingredients</li>
				    <li>Eagerly awaiting your orders!</li>
				    </ul>
				    <Link to="/products">
				    	<Button variant="primary" to="/products" className="btnpress">Check out our menu</Button>
				    </Link>

			    </Col>
			  </Row>
			 </Container>
			 <hr/>
			 <Highlights />
			 { !user.isAdmin? 		
			 	<CartButton />
			 	 : 
			 	 <></>}
		</Fragment>
	)
}