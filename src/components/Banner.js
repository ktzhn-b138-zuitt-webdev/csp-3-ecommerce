import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Fragment } from 'react';

export default function Banner({data}){
	console.log(data)
	const {title, content} = data;
	
		return (
		<div className ="shadow bannImg mx-auto p-5">
			<Row className="mx-auto text-center text-light ">
				<Col className="shadow p-5">
					<h1 className="bannTxt">{title}</h1>
					<p>{content}</p>
					{/*<Link to={destination}>{label}</Link>*/}
				</Col>
			</Row>
		</div>
		)
}