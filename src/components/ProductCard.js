import { Fragment, useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types'
import { Card, Button, Modal, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductCard({productProp, setProducts}) {
	const {_id, name, description, price, isActive} = productProp;
    const { user } = useContext(UserContext);
    const [id, setId] = useState("")
    const [prodName, setprodName] = useState("")
    const [prodDescription, setprodDescription] = useState("")
    const [edit, setEdit] = useState(false)
    const [prodPrice, setprodPrice] = useState(0)
    const [numberOf, setAmountOf] = useState(1)


    const addToCart = () => {
        let productIndex
        let inCart = false
        let cart = []

        if(localStorage.getItem('cart')){
            cart = JSON.parse(localStorage.getItem('cart'))
        }

        for(let i = 0; i < cart.length; i++){
            if(cart[i].productId === _id){
                inCart = true
                productIndex = i
            }
        }

        if(inCart){
            cart[productIndex].numberOf += numberOf
        }else{
            cart.push({
                'productId' : _id,
                'name': name,
                'price': price,
                'numberOf': numberOf,
            })      
        }

        localStorage.setItem('cart', JSON.stringify(cart))

        if(numberOf === 1){
            Swal.fire({
                title: "Added to cart!",
                icon: "success",
                text: "Item added to cart!"
            });
        }else{
            Swal.fire({
                title: "Added to cart!",
                icon: "success",
                text: `${numberOf} items added to cart!`
            });
        }

    }

    const openEdit = (productId) => {

        setId(productId)

        fetch(`https://desolate-dusk-90349.herokuapp.com/products/${ productId }`)
        .then(res => res.json())
        .then(data => {
            setprodName(data.name)
            setprodDescription(data.description)
            setprodPrice(data.price)
        })

        setEdit(true)
    }

    const closeEdit = () => {
        setprodName("")
        setprodDescription("")
        setprodPrice(0)
        setEdit(false)
    }

    const editProduct = (e, productId) => {

        e.preventDefault()

        fetch(`https://desolate-dusk-90349.herokuapp.com/products/${ productId }`, {
        method: 'PUT',
        headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: prodName,
            description: prodDescription,
            price: prodPrice
        })
    })
    .then(res => res.json())
    .then(data => {
        if(data === true){
            Swal.fire({
                title: "Product updated!",
                icon: "success",
                text: "Successfully updated product"
            });
            setprodName("")
            setprodDescription("")
            setprodPrice(0)
            closeEdit()
        }else{
            Swal.fire({
                title: "Error",
                icon: "error",
                text: "Unable to update product"
            });
            closeEdit()
        }
    })
    }


    const archiveProduct = (productId) => {
        fetch(`https://desolate-dusk-90349.herokuapp.com/products/${productId}/archive`, {
        method: 'PUT',
        headers: {
            Authorization: `Bearer ${ localStorage.getItem('token') }`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                fetch('https://desolate-dusk-90349.herokuapp.com/products')
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                })
                Swal.fire({
                    title: "Product Archived",
                    icon: "success",
                    text: "You have successfully archived this product"
                });
            } else{
                Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "Failed! Try again!"
                });
            }
        })
    }

    const activateProduct = (productId) => {
            fetch(`https://desolate-dusk-90349.herokuapp.com/products/${productId}/archive`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                isActive: true
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data === true){
                fetch('https://desolate-dusk-90349.herokuapp.com/products')
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                })
                Swal.fire({
                    title: "Product Activated",
                    icon: "success",
                    text: "You have successfully activated this product"
                });


            }else{
                Swal.fire({
                    title: "Error",
                    icon: "error",
                    text: "Failed! Try again!"
                });
            }
        })
    }

    const minus = () => {
        if (!numberOf <= 0){
            setAmountOf(
                numberOf - 1
            )            
        }
    }
    const plus = () => {
        setAmountOf(
            numberOf + 1
        )            
    }

    return (
        <>
        {!user.isAdmin ? 
        <Card className="mb-3 mx-auto text-center productCard" style={{ width: '18rem' }}>
            <Card.Img variant="top" src="http://lorempixel.com/400/200/food/Happy Cafe" />
            <Card.Body >
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text className="price">₱ {price}</Card.Text>

                <button className="btn-dark" onClick={minus}> − </button>
                <input className="text-center" type="number" value={numberOf} id="input"/>
                <button className="btn-dark" onClick={plus}> + </button>
                 <Link className="btn btn-danger btnpress" onClick={addToCart}>Add to Cart </Link>
                 
            </Card.Body>
        </Card> :
            <>
            <tr key={_id}>

              <td>{name}</td>
              <td>{description}</td>
              <td>{price}</td>
              <td>
                    <Button variant="primary" size="sm" onClick={() => openEdit(productProp._id)}>Update</Button>

                  {productProp.isActive==true
                            ?
                            <Button variant="danger" size="sm" onClick={() => archiveProduct(_id)}>Disable</Button>
                            :
                             <Button variant="success" size="sm" onClick={() => activateProduct(_id)}>Enable</Button>
                        }
{/*                  <button className="btn-danger btnpress" > Delete </button>*/}
              </td>
            </tr>
            <Modal show={edit} onHide={closeEdit}>
                <Form onSubmit={e => editProduct(e, id)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Product</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Form.Group controlId="productName">
                                <Form.Label>Name:</Form.Label>
                                <Form.Control type="text" placeholder="Enter product name" value={prodName} onChange={e => setprodName(e.target.value)} required/>
                            </Form.Group>

                            <Form.Group controlId="productDescription">
                                <Form.Label>Description:</Form.Label>
                                <Form.Control type="text" placeholder="Enter product description" value={prodDescription} onChange={e => setprodDescription(e.target.value)} required/>
                            </Form.Group>

                            <Form.Group controlId="productPrice">
                                <Form.Label>Price:</Form.Label>
                                <Form.Control type="number" placeholder="Enter product price" value={prodPrice} onChange={e => setprodPrice(e.target.value)} required/>
                            </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeEdit}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form> 
            </Modal> 
            </>

        }
        </>
    )
}

ProductCard.propTypes = {
	product: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}